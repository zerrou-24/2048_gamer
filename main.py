from random import *
from time import sleep
from generation import Generation
from gamegrid import GameGrid
from mockgame import MockGameGrid
from score import Score
from consolegame import ConsoleGameGrid
from dna import DNA
import numpy as np

import configparser
import random
import gc

# ML Constants

GENERATIONS_TO_TEST = 1000
WINNERS_TO_CHOOSE = 5
ONE_DNA_PLAY_GAMES = 10
VARIATION_MAX = 100
VARIATION_MIN = -100
MOVES_DEPTH = 2
MODE = 'CONSOLE_GAME' # GAME or MOCK or CONSOLE_GAME
SHOW_CONSOLE_GAMES = False

UP = 100
LEFT = 101
DOWN = 102
RIGHT = 103

def calculateDNAFitness(gameResults):
    result = 0

    for x in range(len(gameResults)):
        #i guess it shoild be * inspite of +
        #if so - don't forget to change default resut to 1
        result = result + scoreMapper(gameResults[x])
    return result

def scoreMapper(number):
    if(number == 2):
        return 1
    elif(number == 4):
        return 4
    elif(number == 8):
        return 10
    elif(number == 16):
        return 25
    elif(number == 32):
        return 70
    elif(number == 64):
        return 200
    elif(number == 128):
        return 600
    elif(number == 256):
        return 1500
    elif(number == 512):
        return 4000
    elif(number == 1024):
        return 9000
    elif(number == 2048):
        return 21000
    elif(number == 4096):
        return 45000
    elif(number == 8192):
        return 100000
    else:
     return -1

def performGame(dna):

    #temp, while game is not implemented
    
    if MODE == 'MOCK':
        gamegrid = MockGameGrid(dna)
    elif MODE == 'CONSOLE_GAME':
        gamegrid = ConsoleGameGrid(dna,SHOW_CONSOLE_GAMES)
    else:
        gamegrid = GameGrid(dna)

    #should be changed for beeing able to launch multiple games in 1 time
    scoreGrid = dna

    #print('***************')
    #print('Performing a new game with dna: %s' % scoreGrid)

    gameResultsArray = []

    while(gamegrid.gameOver == False):
        
        gameResultsArray = []

        #get current grid
        for i in range(len(gamegrid.matrix)):
            for j in range(len(gamegrid.matrix[i])):
                gameResultsArray.append(gamegrid.matrix[i][j])

        #find out best movement, based on current DNA and perform movement
        gamegrid.performMove(getBestMove(gameResultsArray,dna))

        #sleep - just to see game progress :) remove to get boost in learning
        #sleep(0.1)

        #continue, untill game is over
 
    # need to debug it

    #get current grid
    gameResultsArray = []
    for i in range(len(gamegrid.matrix)):
        for j in range(len(gamegrid.matrix[i])):
            gameResultsArray.append(gamegrid.matrix[i][j])


    if (gamegrid.matrix[0][0] == gamegrid.matrix[0][1] or gamegrid.matrix[0][1] == gamegrid.matrix[0][2] or gamegrid.matrix[0][2] == gamegrid.matrix[0][3]):
        sleep(0.1)

    if(gameResultsArray[0] == gameResultsArray[1] or gameResultsArray[1] == gameResultsArray[2] or gameResultsArray[2] == gameResultsArray[3]):
        sleep(0.1)        

    gamegrid.cleanResources()
    gamegrid.destroy()
    del gamegrid

    return gameResultsArray

def getBestMove(grid, dna):

    movements = ['UP', 'DOWN', 'LEFT', 'RIGHT']
    movementScores = []

    for i in range(len(movements)):
        movementScore = Score(movements[i], getScore(getNextGrid(grid, movements[i]),dna)) 
        movementScores.append(movementScore) 
        if not isMoveValid(grid, movements[i]):
            movementScore.score = -999999999

    maxScore = findBestScore(movementScores)[0].score

    return findBestScore(movementScores)[0].movement

def getScore(grid, dna):
    score = 0

    for i in range(len(grid)):
        score += scoreMapper(grid[i]) * dna[i]
    
    return score

def getNextGrid(grid, move):
    
    temp = [0,0,0,0,
            0,0,0,0,
            0,0,0,0,
            0,0,0,0]

    if move == 'UP':
        for i in range (4):
            row = []
            for j in range(4):
                row.append(grid[i + 4*j])
            row = swipeRow(row)
            for j, val in enumerate(row):
                temp[i + 4*j] = val
    elif move == 'LEFT':
        for i in range (4):
            row = []
            for j in range (4):
                row.append(grid[4*i + j])
            row = swipeRow(row)
            for j, val in enumerate(row):
                temp[4*i + j] = val

    elif move == 'DOWN':
        for i in range (4):
            row = []
            for j in range (4):
                row.append(grid[i + 4 * (3 - j)])
            row = swipeRow(row)
            for j, val in enumerate(row):
                temp[i + 4 * (3 - j)] = val

    elif move == 'RIGHT':
        for i in range (4):
            row = []
            for j in range (4):
                row.append(grid[4 * i + (3-j)])
            row = swipeRow(row)
            for j, val in enumerate(row):
                temp[4 * i + (3-j)] = val

    return temp

def swipeRow(row):
    prev = -1
    i = 0
    temp = [0,0,0,0]

    for element in row:
        if element != 0:
            if prev == -1:
                prev = element
                temp[i] = element
                i += 1
            elif prev == element:
                temp[i-1] = 2*prev
                prev = -1
            else: 
                prev = element
                temp[i] = element
                i += 1
    
    return temp

def isMoveValid(grid, move):

    nextGrid = getNextGrid(grid, move)

    if nextGrid == grid:
        return False
    else:
        return True

def findGenerationWinners(generation):
    generation.dnaCollection = bubble_sort(generation.dnaCollection)

    winnersArray = []

    for i in range(WINNERS_TO_CHOOSE):
        winnersArray.append(generation.dnaCollection[i])

    return winnersArray

def bubble_sort(dnaCollection):
    for i in range(len(dnaCollection)):
        for j in range(len(dnaCollection) - 1, i, -1):
            if dnaCollection[j].fitness > dnaCollection[j-1].fitness:
                dnaCollection[j], dnaCollection[j-1] = dnaCollection[j-1], dnaCollection[j]

    return dnaCollection
    
def findBestScore(scoresCollection):
    for i in range(len(scoresCollection)):
        for j in range(len(scoresCollection) - 1, i, -1):
            if scoresCollection[j].score > scoresCollection[j-1].score:
                scoresCollection[j], scoresCollection[j-1] = scoresCollection[j-1], scoresCollection[j]

    return scoresCollection

def findMaxNumber(fieldData):
    tmpArray = []

    for k in range(len(fieldData)):
     tmpArray.append(fieldData[k])

    
    for i in range(len(tmpArray)):
        for j in range(len(tmpArray) - 1, i, -1):
            if tmpArray[j] > tmpArray[j-1]:
                tmpArray[j], tmpArray[j-1] = tmpArray[j-1], tmpArray[j]

    return tmpArray    

def generateNewGeneration(winnersArray):
    newGeneration = Generation(VARIATION_MIN, VARIATION_MAX)
    #todo:
    #random length of winners - add with random new dna's
    for x in range(len(winnersArray)):

        #random cut part of success dna

        partToCut = random.randint(1,len(winnersArray[x].dna)-1)

        firstPart = []
        tmpDNAOne = []
        secondPart = []
        tmpDNATwo = []

        for y in range(len(winnersArray[x].dna)):
            if y >= partToCut:
                firstPart.append(winnersArray[x].dna[y])
                tmpDNAOne.append(winnersArray[x].dna[y])
            else:
                secondPart.append(winnersArray[x].dna[y])   

        #create random new part of child dna_1

        for z in range(len(winnersArray[x].dna)-len(firstPart)):
            #generate radnom tail to child dna_1
            tmpDNAOne.append(random.randint(VARIATION_MIN,VARIATION_MAX)) 

        #create random new part of child dna_2
        for w in range(len(winnersArray[x].dna)-len(secondPart)):
            #generate random head dna_1
            tmpDNATwo.append(random.randint(VARIATION_MIN,VARIATION_MAX))

        for q in range(len(secondPart)):
            #and now add tail to child dna_2
            tmpDNATwo.append(secondPart[q])  

        tmpDna1 = DNA(VARIATION_MIN, VARIATION_MAX)
        tmpDna1.dna = tmpDNAOne

        tmpDna2 = DNA(VARIATION_MIN, VARIATION_MAX)
        tmpDna2.dna = tmpDNATwo

        newGeneration.dnaCollection.append(tmpDna1)
        newGeneration.dnaCollection.append(tmpDna2)

    #add old and new - to generation
    for x in range(len(winnersArray)):
        tmpDna = DNA(VARIATION_MIN, VARIATION_MAX)
        tmpDna.dna = winnersArray[x].dna
        newGeneration.dnaCollection.append(tmpDna)

    #also generate some fully random dna's
    for x in range(3):
        dna = DNA(VARIATION_MIN, VARIATION_MAX)
        newGeneration.dnaCollection.append(dna)

    return newGeneration

def mainGame():
    generationNumber = 0

    #check config file. If exists - continue learning. If no - start from new one
    config = configparser.ConfigParser()
    try:
        config.read('learningstate.ini')
        config.get('main', 'generationNumber')
    except:
        config.read('learningstate.ini')
        config.add_section('main')
        config.set('main', 'generationNumber', str(generationNumber))
        config.set('main', 'winner-1', '')
        config.set('main', 'winner-2', '')
        config.set('main', 'winner-3', '')
        with open('learningstate.ini', 'w') as f:
            config.write(f)

    generationNumber = int(config.get('main', 'generationNumber'))

    # generate or load first generetaion
    gen = Generation(VARIATION_MIN,VARIATION_MAX) 
    gen.generateRandomGeneration()

    if generationNumber != 0 :
        winnerOne = config.get('main', 'winner-1')
        winnerTwo = config.get('main', 'winner-2')
        winnerThree = config.get('main', 'winner-3')
        
        winnerOne = winnerOne.replace("[","").replace("]","").replace(" ","").split(',')
        winnerTwo = winnerTwo.replace("[","").replace("]","").replace(" ","").split(',')
        winnerThree = winnerThree.replace("[","").replace("]","").replace(" ","").split(',')

        winnerOneForDna = []
        winnerTwoForDna = []
        winnerThreeForDna = []

        for el in winnerOne:
            winnerOneForDna.append(int(el))

        for el in winnerTwo:
            winnerTwoForDna.append(int(el))

        for el in winnerThree:
            winnerThreeForDna.append(int(el))

        gen.dnaCollection[0].dna = winnerOneForDna
        gen.dnaCollection[1].dna = winnerTwoForDna
        gen.dnaCollection[2].dna = winnerThreeForDna

    for x in range(GENERATIONS_TO_TEST):
        # launch a game for each dna
        for dna in gen.dnaCollection:
            ## perform a game and calculate game fitness
            for i in range(ONE_DNA_PLAY_GAMES):
                gameReslt = performGame(dna.dna)
                dna.fitnessValues.append(calculateDNAFitness(gameReslt)) 
                dna.maxNumber.append(findMaxNumber(gameReslt)[0])

                #open stats file
                file = open("testfile.txt","a") 
                file.write("%s;%s;%s;%s;%s\n" % (generationNumber+x+1,dna.dna,dna.fitnessValues[i],dna.maxNumber[i],gameReslt))
                file.close()
                del file
            
            dna.fitness = np.mean(dna.fitnessValues)
            #print("Game for dna finished. Score is: %s" % dna.fitness)
        
            #print("Generation %s scores: %s" % (x, dna.fitness))

        # find top performers DNA and generate from them new population

        print("Generation %s winners fintess:" % str(generationNumber+x+1)) 
    
        winnersArray = findGenerationWinners(gen)

        config.read('learningstate.ini')
        config.set('main', 'generationNumber', str(generationNumber+x+1))
        config.set('main', 'winner-1', str(winnersArray[0].dna))
        config.set('main', 'winner-2', str(winnersArray[1].dna))
        config.set('main', 'winner-3', str(winnersArray[2].dna))
        with open('learningstate.ini', 'w') as f:
            config.write(f)

        for winner in winnersArray:
            print("{0:,}".format(winner.fitness))
    
        gen.cleanResources()
        del gen

        gen = generateNewGeneration(winnersArray)

        del winnersArray
        #debugMem()
        gc.collect()
        del gc.garbage[:]
        #debugMem()

def debugMem():
    #print("\n MEMORY DEBUG:")
    import objgraph

    #objgraph.show_most_common_types()
    
    print("\n GROWTH:") 

    objgraph.show_growth()

    print("\n END \n") 

mainGame()