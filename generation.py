from dna import DNA

class Generation(object):
    """docstring"""
 
    def __init__(self,VARIATION_MIN,VARIATION_MAX):
        """Constructor"""
        self.dnaCollection = []
        self.VARIATION_MIN = VARIATION_MIN
        self.VARIATION_MAX = VARIATION_MAX

    def cleanResources(self):
        for i in range(len(self.dnaCollection)):
            self.dnaCollection[i].cleanResources
        del self.dnaCollection
    
    def generateRandomGeneration(self):
        for x in range(10):
            dna = DNA(self.VARIATION_MIN, self.VARIATION_MAX)
            self.dnaCollection.append(dna)