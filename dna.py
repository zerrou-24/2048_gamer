from random import *
import random


class DNA(object):  
    """docstring"""
 
    def __init__(self, VARIATION_MIN, VARIATION_MAX):
        """Constructor"""
        self.fitness = 0
        self.fitnessValues = []
        self.maxNumber = []
        self.VARIATION_MIN = VARIATION_MIN
        self.VARIATION_MAX = VARIATION_MAX
        self.dna = self.generateRandomDNA()


    def cleanResources(self):
        del self.dna
        del self.fitness
        del self.maxNumber

    def generateRandomDNA(self):
        resultArray = []
        for x in range(16):
            resultArray.append(random.randint(self.VARIATION_MIN,self.VARIATION_MAX)) 
        return resultArray