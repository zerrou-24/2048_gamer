from random import *
import random
import os

class ConsoleGameGrid():

    gameOver = False

    def cleanResources(self):
        del self.matrix
        
    def __init__(self, dna, showGames):
        self.matrix = self.generateNewGameField()
        self.showGames = showGames
        self.displayGame()

    def generateNewGameField (self):
        matrix = [[0,0,0,0],[0,0,0,0],[0,0,0,0],[0,0,0,0]]

        firstNumberRow = random.randint(0,3)
        firstNumberColumn = random.randint(0,3)

        secondNumberRow = random.randint(0,3)
        secondNumberColumn = random.randint(0,3)

        matrix[firstNumberRow][firstNumberColumn] = 2
        matrix[secondNumberRow][secondNumberColumn] = 2

        return matrix

    def performMove(self, movement):        
        self.gameOver = False

        grid = []

        for i in range(4):
            for j in range(4):
                grid.append(self.matrix[i][j])

        newGrid = []
        #perform move
        if movement == 'UP':
            newGrid = self.getNextGrid(grid,'UP')
        elif movement == 'DOWN':
            newGrid = self.getNextGrid(grid,'DOWN')
        elif movement == 'LEFT':
            newGrid = self.getNextGrid(grid,'LEFT')
        else:
            newGrid = self.getNextGrid(grid,'RIGHT')         

        if grid != newGrid:
            #save this grid as new one
            counter = 0
            for i in range(4):
                for j in range(4):
                    self.matrix[i][j] = newGrid[counter]
                    counter += 1

        else:
            #move is prohibited
            self.gameOver = True
            return

        #safety check
        emptyFields = 0

        for i in range(4):
            for j in range(4):
                if self.matrix[i][j] == 0:
                    emptyFields += 1
                
        if emptyFields==0:
            self.gameOver = True
            return

        #generate new random points

        newNumberPositionNotFound = True

        # shoud modify it, It won't work if there are few empty fields
        while (newNumberPositionNotFound):
            newNumberRow = random.randint(0,3)
            newNumberColumn = random.randint(0,3)
            if(self.matrix[newNumberRow][newNumberColumn] == 0):
                newNumberPositionNotFound = False
                self.matrix[newNumberRow][newNumberColumn] = 2

        self.displayGame()
        return

    def getNextGrid(self, grid, move):
        
        temp = [0,0,0,0,
                0,0,0,0,
                0,0,0,0,
                0,0,0,0]

        if move == 'UP':
            for i in range (4):
                row = []
                for j in range(4):
                    row.append(grid[i + 4*j])
                row = self.swipeRow(row)
                for j, val in enumerate(row):
                    temp[i + 4*j] = val
        elif move == 'LEFT':
            for i in range (4):
                row = []
                for j in range (4):
                    row.append(grid[4*i + j])
                row = self.swipeRow(row)
                for j, val in enumerate(row):
                    temp[4*i + j] = val

        elif move == 'DOWN':
            for i in range (4):
                row = []
                for j in range (4):
                    row.append(grid[i + 4 * (3 - j)])
                row = self.swipeRow(row)
                for j, val in enumerate(row):
                    temp[i + 4 * (3 - j)] = val

        elif move == 'RIGHT':
            for i in range (4):
                row = []
                for j in range (4):
                    row.append(grid[4 * i + (3-j)])
                row = self.swipeRow(row)
                for j, val in enumerate(row):
                    temp[4 * i + (3-j)] = val

        return temp
         
    def swipeRow(self, row):
        prev = -1
        i = 0
        temp = [0,0,0,0]

        for element in row:
            if element != 0:
                if prev == -1:
                    prev = element
                    temp[i] = element
                    i += 1
                elif prev == element:
                    temp[i-1] = 2*prev
                    prev = -1
                else: 
                    prev = element
                    temp[i] = element
                    i += 1
        
        return temp

    def destroy(self):
        self.matrix = []

    def displayGame(self):
        if(self.showGames == True):
            os.system('clear')
            print(self.matrix[0])
            print(self.matrix[1])
            print(self.matrix[2])
            print(self.matrix[3])
