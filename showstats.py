import numpy as np
import pandas as pd
from matplotlib import pyplot
from pandas import read_csv
from time import sleep

while(True):
    #get data
    def GetData(fileName):
        data = read_csv(fileName, sep=';', header=None, names=['GENERATION','DNA','FITNESS','MAX_NUMBER','GAME_LOG'])
        #print(data.index[0])
        return data

    actualData = GetData('testfile.txt')

    #calcuate fitness/generation stats
    gamesFitnessResult = (actualData[['GENERATION','FITNESS']]).groupby(['GENERATION']).agg({'max'})

    #calculate more games with more than 512 score

    games512Result = (actualData[['GENERATION','MAX_NUMBER']]).groupby(['GENERATION']).agg({'max'})

    topSurvivedDNAResult = (actualData[['DNA']]).groupby(['DNA']).size().reset_index(name='count').sort_values(['count'], ascending=False)

    print(topSurvivedDNAResult.head())

    pyplot.subplot(2, 1, 1)

    pyplot.title('Fitness/Generation stats')

    pyplot.plot(gamesFitnessResult, color='green')

    pyplot.subplot(2, 1, 2)

    #pyplot.title('Games wih score more than 512')
    pyplot.title('Max score in generations')

    pyplot.plot(games512Result, color='green')

    pyplot.show(block = False)

#    pyplot.show()
    pyplot.pause(15)

    pyplot.close()
