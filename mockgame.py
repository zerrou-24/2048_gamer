
class MockGameGrid():

    gameOver = False

    def cleanResources(self):
        del self.matrix
        
    def __init__(self, dna):
        self.matrix = [[dna[0],dna[1],dna[2],dna[3]],[dna[4],dna[5],dna[6],dna[7]],[dna[8],dna[9],dna[10],dna[11]],[dna[12],dna[13],dna[14],dna[14]]]

    def performMove(self, movement):        
        self.gameOver = True

    def destroy(self):
        self.matrix = []